import { Component, OnInit } from '@angular/core';
import { EditaccService } from '../editacc.service';
import { MessageService } from '../message.service';
import { MyResponse } from '../MyResponse';
import { Account } from '../account'
import { Router } from '@angular/router';

@Component({
  selector: 'app-editacccomp',
  templateUrl: './editacccomp.component.html',
  styleUrls: ['./editacccomp.component.css']
})
export class EditacccompComponent implements OnInit {
  acc: Account;
  showFlag: boolean = true;
  showForm:boolean=false;
  public message: MyResponse = { "status": "not done" };

  constructor(public editAccount:EditaccService,private router: Router, private mesageService: MessageService) { }

  ngOnInit() {
  }
  onSubmit() {
    this.editAccount.saveAcc(this.acc).subscribe(data => this.message = data,error=>console.log(error),()=>this.handleMessage());

  }
  toggle(): void {
    this.showFlag = true;
  }

  doEdit():void{
    this.showFlag = false;
    this.showForm=true;
  }
  onSubmit2() {
    console.log("data submitted")
    this.showFlag = false;
    this.showForm=false;
    this.editAccount.saveAcc(this.editAccount.acc).subscribe(data => this.message = data,error=>console.log(error),()=>this.handleMessage());

  }
  handleMessage() {
    this.mesageService.add(this.message);
    console.log(this.message.status);
    this.router.navigate(['/message']);
  }

}
