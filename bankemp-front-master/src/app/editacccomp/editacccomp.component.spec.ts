import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditacccompComponent } from './editacccomp.component';

describe('EditacccompComponent', () => {
  let component: EditacccompComponent;
  let fixture: ComponentFixture<EditacccompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditacccompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditacccompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
