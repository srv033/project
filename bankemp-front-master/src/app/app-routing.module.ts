import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddempComponent } from './addemp/addemp.component';

import { DashboardComponent } from './dashboard/dashboard.component';

import { ListComponent } from './list/list.component';
import { MessageComponent } from './message/message.component';
import { AddcusComponent } from './addcus/addcus.component';
import { CuslistComponent } from './cuslist/cuslist.component';
import { DeletecusComponent } from './deletecus/deletecus.component';
import { UpdateAccountComponent } from './update-account/update-account.component';
import { EditacccompComponent } from './editacccomp/editacccomp.component';

const routes: Routes = [{path:'addemp',component:AddempComponent},
{path:'addcus',component:AddcusComponent},

{path:'update',component:UpdateAccountComponent},
{path:'list',component:ListComponent},
{path:'editacc',component:EditacccompComponent},
{path:'message',component:MessageComponent},
{path:'cus',component:CuslistComponent},
{path:'del',component:DeletecusComponent},
{path:'',component:DashboardComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
