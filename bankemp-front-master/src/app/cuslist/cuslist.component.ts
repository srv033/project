import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Customer } from '../Customer';

@Component({
  selector: 'app-cuslist',
  templateUrl: './cuslist.component.html',
  styleUrls: ['./cuslist.component.css']
})
export class CuslistComponent implements OnInit {
public cusList:Customer[]; 
  constructor(private _service:EmpService) { }

  ngOnInit() {
    
    this._service.getCusList().subscribe(data=>this.cusList=data);
    this._service.getCusList().subscribe(data=>console.log(data));
    
    
    
  }

}
