import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletecusComponent } from './deletecus.component';

describe('DeletecusComponent', () => {
  let component: DeletecusComponent;
  let fixture: ComponentFixture<DeletecusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletecusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletecusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
