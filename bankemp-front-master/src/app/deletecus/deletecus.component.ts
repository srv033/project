import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Account } from '../account';
import { MessageService } from '../message.service';
import { MyResponse } from '../MyResponse';
import { Router } from '@angular/router';
//import { Router } from '@angular/router';

@Component({
  selector: 'app-deletecus',
  templateUrl: './deletecus.component.html',
  styleUrls: ['./deletecus.component.css']
})
export class DeletecusComponent implements OnInit {

  public account_Id: number;
  public acc:Account=new Account();//={account_Id:100,account_Type:'',account_Balance:1,branch:'',customer_Id:1,status:0,opened_Date:''};
  public message: MyResponse = { "status": "not done" };

  constructor(private _service:EmpService,private router:Router, private mesageService: MessageService) { }

  ngOnInit() {
  }
  onSubmit(){
    this._service.delete(this.account_Id).subscribe(data=>this.message=data,error=>console.log(error),()=>this.handleMessage());
    
  }

  handleMessage() {
    this.mesageService.add(this.message);
    console.log(this.message.status);
    this.router.navigate(['/message']);
  }

}
