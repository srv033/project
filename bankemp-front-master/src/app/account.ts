export class Account{
    account_Id :number;
    branch :string;
    customer_Id :number;
    account_Balance :number;
    account_Type :string;
    opened_Date:string;
    status :number;
}



// +-----------------+--------------+------+-----+---------+-------+
// | Field           | Type         | Null | Key | Default | Extra |
// +-----------------+--------------+------+-----+---------+-------+
// | account_Id      | int(10)      | NO   | PRI | NULL    |       |
// | branch          | varchar(100) | NO   |     | NULL    |       |
// | customer_Id     | int(10)      | NO   |     | NULL    |       |
// | account_Balance | int(10)      | NO   |     | NULL    |       |
// | account_Type    | varchar(10)  | NO   |     | NULL    |       |
// | opened_Date     | date         | NO   |     | NULL    |       |
// | status          | int(1)       | NO   |     | NULL    |       |
// +-----------------+--------------+------+-----+---------+-------+