import { Injectable } from '@angular/core';
import {Account} from './account';
import { Observable } from 'rxjs';
import { MyResponse } from './MyResponse';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EditaccService {
  private base_url="http://localhost:8080/";

  public acc:Account;
  constructor(private http:HttpClient) { }
  public add(acc1:Account){
    console.log('from Service '+acc1.branch);
    this.acc = acc1;
  }
  saveAcc(acc2: Account):Observable<MyResponse> {
    console.log('saveAcc called...');
    return this.http.put<MyResponse>(this.base_url+'acc/update',acc2)
  }

}
