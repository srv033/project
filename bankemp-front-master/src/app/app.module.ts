import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddempComponent } from './addemp/addemp.component';
//import { FindempComponent } from './findemp/findemp.component';
//import { UpdateEmpComponent } from './update-acc/update-acc.component';
import { ListComponent } from './list/list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmpService } from './emp.service';
import { MessageComponent } from './message/message.component';
import { MessageService } from './message.service';
import { AddcusComponent } from './addcus/addcus.component';
import { CuslistComponent } from './cuslist/cuslist.component';
import { DeletecusComponent } from './deletecus/deletecus.component';
import { UpdateAccountComponent } from './update-account/update-account.component';
import { EditacccompComponent } from './editacccomp/editacccomp.component';
import { EditaccService } from './editacc.service';

@NgModule({
  declarations: [
    AppComponent,
    AddempComponent,
    
    ListComponent,
    DashboardComponent,
    MessageComponent,
    AddcusComponent,
    CuslistComponent,
    DeletecusComponent,
    UpdateAccountComponent,
    EditacccompComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [EmpService,MessageService,EditaccService],
  bootstrap: [AppComponent]
})
export class AppModule { }
