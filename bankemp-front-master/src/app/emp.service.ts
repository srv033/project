import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Emp } from './emp';
import { MyResponse } from "./MyResponse";
import { Customer } from './Customer';
import {Account} from './account';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  
  

  private base_url="http://localhost:8080/";

  constructor(private http:HttpClient) { }

  public saveCus(cusModel: Customer):Observable<MyResponse> {
    console.log('saveCustomer called...');
    return this.http.post<MyResponse>(this.base_url+'customer/update',cusModel)
  }
  public getEmpList(): Observable<Emp[]>{
    return this.http.get<Emp[]>(this.base_url+'emp/list1')
  }

  public getCusList(): Observable<Customer[]>{
    return this.http.get<Customer[]>(this.base_url+'customer/list1')
  }

  saveEmp(empModel: Emp):Observable<MyResponse> {
    console.log('saveEmp called...');
    return this.http.post<MyResponse>(this.base_url+'emp/save',empModel)
  }
 

  
  find(account_Id: number): Observable<Account> {
    return this.http.get<Account>(this.base_url+'acc/find/'+account_Id);
  }
  delete(account_Id: number): Observable<MyResponse> {
    return this.http.get<MyResponse>(this.base_url+'acc/delete/'+account_Id);
  }


}
