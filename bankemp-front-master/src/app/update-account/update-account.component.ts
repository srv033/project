import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Account } from '../account';
import { Router } from '@angular/router';
import { EditaccService } from '../editacc.service';


@Component({
  selector: 'app-update-account',
  templateUrl: './update-account.component.html',
  styleUrls: ['./update-account.component.css']
})
export class UpdateAccountComponent implements OnInit {
  public acc: Account;//new Account();//={account_Id:100,account_Type:'',account_Balance:1,branch:'',customer_Id:1,status:0,opened_Date:''};
  public account_Id: number;

  constructor(private _service: EmpService, private router: Router, private editAccount: EditaccService) { }


  ngOnInit() {
    this.account_Id = 1000;
  }

  onSubmit() {
    this._service.find(this.account_Id).subscribe(data=>this.acc=data);
    this._service.find(this.account_Id).subscribe(data => console.log(data));
    this.editAcc();

  }

  editAcc() {
    console.log("i am here..")
    this.editAccount.add(this.acc);
    this.router.navigate(['/editacc'])
  }

}
