import { Component, OnInit } from '@angular/core';
import { Customer } from '../Customer';
import { MyResponse } from '../MyResponse';
import { Router } from '@angular/router';
import { MessageService } from '../message.service';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-addcus',
  templateUrl: './addcus.component.html',
  styleUrls: ['./addcus.component.css']
})
export class AddcusComponent implements OnInit {

  public cusModel: Customer = new Customer();
  public message: MyResponse = { "status": "not done" };
  constructor(private _service: EmpService, private router: Router, private mesageService: MessageService) { }
  

  ngOnInit() {
  //this.cusModel = { customer_Id: 2000, name: '....', mail: '....', address: '....',dob:'....',gender:'....' };
  }
  public onSubmit() {
    this._service.saveCus(this.cusModel).subscribe(data => this.message = data,error=>console.log(error),()=>this.handleMessage());

  }

  handleMessage() {
    this.mesageService.add(this.message);
    console.log(this.message.status);
    this.router.navigate(['/message']);
  }

}
