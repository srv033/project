package com.demo.spring;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.spring.entity.account;

public interface AccountRepository extends JpaRepository<account, Integer> {

}
