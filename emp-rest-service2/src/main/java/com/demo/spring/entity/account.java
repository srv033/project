package com.demo.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="account")
public class account {

@Id
@Column(name="account_Id")
private Integer account_Id;
private String branch;
@Column(name="customer_Id")
private Integer customer_Id;
@Column(name="account_Balance")
private Integer account_Balance;
@Column(name="account_Type")
private String account_Type;
@Column(name="opened_Date")
private String opened_Date;
private Integer status;
public account() {
	// TODO Auto-generated constructor stub
}
public account(Integer account_Id, String branch, Integer customer_Id, Integer account_Balance, String account_Type,
		String opened_Date, Integer status) {
	this.account_Id = account_Id;
	this.branch = branch;
	this.customer_Id = customer_Id;
	this.account_Balance = account_Balance;
	this.account_Type = account_Type;
	this.opened_Date = opened_Date;
	this.status = status;
}
public Integer getAccount_Id() {
	return account_Id;
}
public void setAccount_Id(Integer account_Id) {
	this.account_Id = account_Id;
}
public String getBranch() {
	return branch;
}
public void setBranch(String branch) {
	this.branch = branch;
}
public Integer getCustomer_Id() {
	return customer_Id;
}
public void setCustomer_Id(Integer customer_Id) {
	this.customer_Id = customer_Id;
}
public Integer getAccount_Balance() {
	return account_Balance;
}
public void setAccount_Balance(Integer account_Balance) {
	this.account_Balance = account_Balance;
}
public String getAccount_Type() {
	return account_Type;
}
public void setAccount_Type(String account_Type) {
	this.account_Type = account_Type;
}
public String getOpened_Date() {
	return opened_Date;
}
public void setOpened_Date(String opened_Date) {
	this.opened_Date = opened_Date;
}
public Integer getStatus() {
	return status;
}
public void setStatus(Integer status) {
	this.status = status;
}

}