package com.demo.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customer")
public class customer{

	@Id
	private Integer customer_Id;
	//private String password;
	private String name;
	private String mail;
	private String address;
	//private Integer phone_No;
	private String dob;
	private String gender;
	public customer() {
		// TODO Auto-generated constructor stub
	}
	
	public customer(Integer customer_Id, String name, String mail, String address, String dob, String gender) {
		this.customer_Id = customer_Id;
		this.name = name;
		this.mail = mail;
		this.address = address;
		this.dob = dob;
		this.gender = gender;
	}
	public Integer getCustomer_Id() {
		return customer_Id;
	}
	public void setCustomer_Id(Integer customer_Id) {
		this.customer_Id = customer_Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
}
